// Fill out your copyright notice in the Description page of Project Settings.

#include "Utility.h"
#include "AITest.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetSystemLibrary.h"
#include "CollisionQueryParams.h"
#include "Engine/World.h"
#include "AICharacter.h"

Utility::Utility()
{
}

Utility::~Utility()
{
}

float Utility::LinearEquation(float x1, float x2, float y1, float y2, float input)
{
	return (y2*(input - x1) - y1*(input - x2)) / (x2 - x1);
}

float Utility::GetHighestValue(TArray<float> values)
{
	float maxValue = 0.0f;
	for (int i = 0; i < values.Num(); i++)
	{
		maxValue = FMath::Max(maxValue, values[i]);
	}
	return maxValue;
}

float Utility::GraphFunction(float minX, float peakMinX, float peakMaxX, float maxX, float maxVal, float input)
{
	if (input <= peakMaxX)
	{
		if (input <= peakMinX)
		{
			return LinearEquation(minX, peakMinX, 0.0f, maxVal, input);
		}
		else
		{
			return maxVal;
		}
	}
	else
	{
		return LinearEquation(peakMaxX, maxX, maxVal, 0.0f, input);
	}
}

float Utility::GetXValue(float minX, float peakMinX, float peakMaxX, float maxX, float maxVal, float yVal, float position)
{
	if (position == 0)
	{
		return LinearEquation(0.0f, maxVal, minX, peakMinX, yVal);
	}
	else
	{
		return LinearEquation(maxVal, 0.0f, peakMaxX, maxX, yVal);
	}
}

AActor* Utility::GetClosestTarget(FVector currentLocation, TArray<AActor*> targets)
{
	FVector targetLoc;
	AActor* target;
	FVector old;
	targetLoc = targets[0]->GetActorLocation();
	target = targets[0];
	for (int i = 1; i < targets.Num(); i++)
	{
		old = targetLoc - currentLocation;
		FVector next = targets[i]->GetActorLocation() - currentLocation;
		if (old.Size() < next.Size())
		{
			targetLoc = targets[i]->GetActorLocation();
			target = targets[i];
		}
	}

	return target;
}

TArray<float> Utility::GetFuzzyValue(TArray<float> fewValues, TArray<float> okValues, TArray<float> muchValues, float maxValue, float input)
{
	TArray<float> returnVal;

	if (input == 0.0f)
	{
		returnVal.Add(maxValue);
	}
	else
	{
		returnVal.Add(GraphFunction(fewValues[0], fewValues[1], fewValues[2], fewValues[3], maxValue, input));
	}
	returnVal.Add(GraphFunction(okValues[0], okValues[1], okValues[2], okValues[3], maxValue, input));
	if (input == muchValues[3])
	{
		returnVal.Add(maxValue);
	}
	else
	{
		returnVal.Add(GraphFunction(muchValues[0], muchValues[1], muchValues[2], muchValues[3], maxValue, input));
	}

	return returnVal;
}

void Utility::AssignFuzzyValues(TArray<EBehaviour> behaviours, TArray<float> &fleeValues, TArray<float> &ignoreValues, TArray<float> &attackValues, TArray<float> fuzzyValues)
{
	for (int i = 0; i < behaviours.Num(); i++)
	{
		switch (behaviours[i])
		{
		case EBehaviour::Flee: fleeValues.Add(fuzzyValues[i]);
			break;
		case EBehaviour::Ignore: ignoreValues.Add(fuzzyValues[i]);
			break;
		case EBehaviour::Attack: attackValues.Add(fuzzyValues[i]);
			break;
		}
	}
}

void Utility::AssignAmmoNeedFuzzyValues(TArray<EANeedBehaviour> behaviours, TArray<float> &noNeedValues, TArray<float> &collectValues, TArray<float> fuzzyValues)
{
	for (int i = 0; i < behaviours.Num(); i++)
	{
		switch (behaviours[i])
		{
		case EANeedBehaviour::NoNeed: noNeedValues.Add(fuzzyValues[i]);
			break;
		case EANeedBehaviour::Collect: collectValues.Add(fuzzyValues[i]);
			break;
		}
	}
}

void Utility::UpdateEnemiesInView(UWorld* world, AActor* character, TArray<AActor*> &enemies, TArray<AActor*> &enemiesInView, TArray<AActor*> &enemiesLeftSight, TArray<float> &timeSinceLeft)
{
	for (int i = 0; i < enemies.Num(); i++)
	{
		if (FVector::DotProduct(character->GetActorForwardVector(), enemies[i]->GetActorLocation() - character->GetActorLocation()) >= 0.0f)
		{
			FHitResult result;
			FCollisionQueryParams traceParams(TEXT("VisionTrace"), true, character);
			traceParams.bTraceAsyncScene = true;
			traceParams.bReturnPhysicalMaterial = true;
			world->LineTraceSingleByChannel(result, character->GetActorLocation(), enemies[i]->GetActorLocation(), COLLISION_PLAYER, traceParams);
			if (result.GetActor() == enemies[i])
			{
				if (enemiesLeftSight.Contains(enemies[i]))
				{
					int32 index;
					enemiesLeftSight.Find(enemies[i], index);
					enemiesLeftSight.RemoveAt(index);
					timeSinceLeft.RemoveAt(index);
				}
				enemiesInView.AddUnique(enemies[i]);
			}
		}
	}
}

void Utility::UpdateEnemiesInMind(UWorld* world, AActor* character, TArray<AActor*> &enemies, TArray<AActor*> &enemiesInView, TArray<AActor*> &enemiesLeftSight, TArray<float> &timeSinceLeft, float DeltaSeconds, float sightDistance)
{
	for (int i = 0; i < enemiesInView.Num(); i++)
	{
		if (FVector::DotProduct(character->GetActorForwardVector(), enemiesInView[i]->GetActorLocation() - character->GetActorLocation()) >= 0.0f)
		{
			FHitResult result;
			FCollisionQueryParams traceParams(TEXT("VisionTrace"), true, character);
			traceParams.bTraceAsyncScene = true;
			traceParams.bReturnPhysicalMaterial = true;
			world->LineTraceSingleByChannel(result, character->GetActorLocation(), enemiesInView[i]->GetActorLocation(), COLLISION_PLAYER, traceParams);
			if (result.GetActor() == enemiesInView[i] && (enemiesInView[i]->GetActorLocation() - character->GetActorLocation()).Size() <= sightDistance)
			{
				continue;
			}
		}
		enemiesLeftSight.AddUnique(enemiesInView[i]);
		timeSinceLeft.Add(5.0f);
		enemiesInView.RemoveAt(i);
		i--;
	}
	for (int i = 0; i < timeSinceLeft.Num(); i++)
	{
		timeSinceLeft[i] -= DeltaSeconds;
		if (timeSinceLeft[i] <= 0.0f)
		{
			enemiesLeftSight.RemoveAt(i);
			timeSinceLeft.RemoveAt(i);
			i--;
		}
	}
}

void Utility::UpdateObjectsInView(UWorld* world, AActor* character, TArray<AActor*> &objects, TArray<AActor*> &objectsInView)
{
	for (int i = 0; i < objects.Num(); i++)
	{
		float f = FVector::DotProduct(character->GetActorForwardVector(), objects[i]->GetActorLocation() - character->GetActorLocation());
		if (FVector::DotProduct(character->GetActorForwardVector(), objects[i]->GetActorLocation() - character->GetActorLocation()) >= 0.0f)
		{
			FHitResult result;
			FCollisionQueryParams traceParams(TEXT("VisionTrace"), true, character);
			traceParams.bTraceAsyncScene = true;
			traceParams.bReturnPhysicalMaterial = true;
			world->LineTraceSingleByChannel(result, character->GetActorLocation(), objects[i]->GetActorLocation(), ECollisionChannel::ECC_Visibility, traceParams);
			if (result.GetActor() == objects[i])
			{
				objectsInView.AddUnique(objects[i]);
			}
		}
		else
		{
			if (objectsInView.Find(objects[i]) != INDEX_NONE)
			{
				objectsInView.Remove(objects[i]);
			}
		}
	}
}