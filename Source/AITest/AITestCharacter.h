// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.h"
#include "AITestCharacter.generated.h"

UCLASS(Blueprintable)
class AAITestCharacter : public ABaseCharacter
{
	GENERATED_BODY()

public:
	AAITestCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	virtual void Destroyed() override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }
	///* Returns SphereComponent subobject */
	//FORCEINLINE class USphereComponent* GetSphereComponent() { return SphereComponent; }

private:
	/** Top down camera **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	UUserWidget* UIRef;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
		TSubclassOf<class UUserWidget> wUI;

	UPROPERTY(BlueprintReadWrite)
		FString mapName;

//	/* Component for register and unregister enemies */
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
//	class USphereComponent* SphereComponent;
//
//	float fleeMinX;
//	float fleeMaxX;
//	float ignoreMinX;
//	float ignoreMaxX;
//	float attackMinX;
//	float attackMaxX;
//	FVector targetLocation;
//	FTimerHandle IdleTimerHandle;
//	bool isIdling;
//	UUserWidget* UIRef;
//
//	void EndIdleWait();
//
//	void DealDamage(AActor* target);
//
//	void Shoot(AActor* target);
//
//public:
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
//		float detectionRadius;
//	/*shot angle variation*/
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
//		float accuracy;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
//		float meleeRange;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
//		TSubclassOf<class UUserWidget> wUI;
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI|Fuzzyfication|defaultValues")
//		float fleeValue;
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI|Fuzzyfication|defaultValues")
//		float ignoreValue;
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI|Fuzzyfication|defaultValues")
//		float attackValue;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|defaultValues")
//		float maxGraphValue;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|defaultValues")
//		float ammo;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|defaultValues")
//		float bravery;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|defaultValues")
//		TArray<class AActor*> enemies;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|defaultValues")
//		TArray<class AActor*> enemiesInView;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|defaultValues")
//		TArray<class AActor*> enemiesLeftSight;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|defaultValues")
//		TArray<float> timeSinceLeft;
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI|Fuzzyfication|defaultValues")
//		float fuzzyAmmo;
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI|Fuzzyfication|defaultValues")
//		float fuzzyBravery;
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI|Fuzzyfication|defaultValues")
//		float fuzzyEnemies;
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI|Fuzzyfication|defaultValues")
//		float resultV;
//
//	/*set border values for behaviour intervall - flee, ignore*/
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|defaultValues")
//		TArray<float> behaviourValues;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|Ammo")
//		TArray<float> valuesAFew;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|Ammo")
//		TArray<float> valuesAOk;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|Ammo")
//		TArray<float> valuesAMuch;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|Bravery")
//		TArray<float> valuesBFew;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|Bravery")
//		TArray<float> valuesBOk;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|Bravery")
//		TArray<float> valuesBMuch;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|Enemies")
//		TArray<float> valuesEFew;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|Enemies")
//		TArray<float> valuesEOk;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Fuzzyfication|Enemies")
//		TArray<float> valuesEMuch;
//
//	/*values are few, ok and much*/
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|FuzzyRules")
//		TArray<EBehaviour> ammoBehaviour;
//	/*values are few, ok and much*/
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|FuzzyRules")
//		TArray<EBehaviour> braveryBehaviour;
//	/*values are few, ok and much*/
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|FuzzyRules")
//		TArray<EBehaviour> enemiesBehaviour;
//
//	/*enter values starting left*/
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|FuzzyRules")
//		TArray<float> valuesFlee;
//	/*enter values starting left*/
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|FuzzyRules")
//		TArray<float> valuesIgnore;
//	/*enter values starting left*/
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|FuzzyRules")
//		TArray<float> valuesAttack;
//
//	TArray<float> GetFuzzyAmmo();
//	TArray<float> GetFuzzyBravery();
//	TArray<float> GetFuzzyEnemies();
//
//	UFUNCTION(BlueprintCallable, Category = "AI|FuzzyRules")
//		void SetFuzzyValues();
//
//	UFUNCTION(BlueprintCallable, Category = "AI|FuzzyInference")
//		void SetValuesForAverageOfMaxima();
//	UFUNCTION(BlueprintCallable, Category = "AI|FuzzyInference")
//		float GetCenterOfGravity();
//
//	UFUNCTION(BlueprintCallable, Category = "AI|Defuzzyfication")
//		void SelectReaction();
//
//	UFUNCTION()
//		void AddEnemy(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
//	UFUNCTION()
//		void RemoveEnemy(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
//
//	void Attack();
//	void Idling();
//	void Flee();
//
//	void SetTargetLocation(FVector goal);
};

