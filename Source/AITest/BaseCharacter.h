// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Utility.h"
#include "BaseCharacter.generated.h"

UENUM()
enum class ETeam : uint8
{
	Red,
	Blue,
};

UCLASS()
class AITEST_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABaseCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	/* Returns SphereComponent subobject */
	FORCEINLINE class USphereComponent* GetSphereComponent() { return SphereComponent; }

private:
	/* Component for register and unregister enemies */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* SphereComponent;
	USkeletalMeshComponent* weaponMesh;
	float health;
	float bravery;
	float ammoNeed;
	float accuracy;
	float meleeRange;
	float ammo;
	float shotFrequency;
	TArray<AActor*> allies;
	TArray<AActor*> enemies;
	TArray<AActor*> enemiesInView;
	TArray<AActor*> enemiesLeftSight;
	TArray<AActor*> consideredTargets;
	TArray<float> timeSinceLeft;
	TArray<AActor*> objects;
	TArray<AActor*> objectsInView;
	float fleeValue;
	float fleeMinX;
	float fleeMaxX;
	float ignoreValue;
	float ignoreMinX;
	float ignoreMaxX;
	float attackValue;
	float attackMinX;
	float attackMaxX;
	float noNeedValue;
	float noNeedMinX;
	float noNeedMaxX;
	float collectValue;
	float collectMinX;
	float collectMaxX;
	FVector targetLocation;
	FTimerHandle IdleTimerHandle;
	FTimerHandle ShotTimerHandle;
	FTimerHandle CoverTimerHandle;
	bool isIdling;
	bool hasShot;
	bool isTargetting;
	bool isCovering;

	void EndIdleWait();
	void EndShot();
	void EndCover();
	void DealDamage(AActor* target);
	void Shoot(AActor* target);
	void Attack();
	void executeAttack(TArray<AActor*> targets);
	void Idling(float minRange, float maxRange, float minWaitTime, float maxWaitTime);
	void Flee();

	TArray<float> GetFuzzyAmmo();
	TArray<float> GetFuzzyHealth();
	TArray<float> GetFuzzyEnemies();

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
		USkeletalMesh* gun;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
		float detectionRadius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
		float baseHealth;
	/*values 0-1 for hit ratio*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
		float baseAccuracy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
		float baseMeleeRange;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
		float baseAmmo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
		float baseShotFrequency;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
		float maxGraphValue;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "defaultValues")
		UParticleSystem* hitParticles;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "defaultValues")
		UParticleSystem* muzzleFlashParticles;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "defaultValues")
		ETeam team;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "defaultValues")
		FName WeaponAttachPoint;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "defaultValues")
		FName MuzzleAttachPoint;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "defaultValues")
		float fleeInfluenceValue;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "defaultValues")
		float ignoreInfluenceValue;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "defaultValues")
		float attackInfluenceValue;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "defaultValues")
		float noNeedInfluenceValue;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "defaultValues")
		float collectInfluenceValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Ammo")
		TArray<float> valuesAFew;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Ammo")
		TArray<float> valuesAOk;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Ammo")
		TArray<float> valuesAMuch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Health")
		TArray<float> valuesHFew;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Health")
		TArray<float> valuesHOk;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Health")
		TArray<float> valuesHMuch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Enemies")
		TArray<float> valuesEFew;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Enemies")
		TArray<float> valuesEOk;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Enemies")
		TArray<float> valuesEMuch;


	/*set border values for behaviour intervall - flee, ignore*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|Basic")
		TArray<float> behaviourValues;
	/*values are few, ok and much*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|Basic")
		TArray<EBehaviour> ammoBehaviour;
	/*values are few, ok and much*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|Basic")
		TArray<EBehaviour> healthBehaviour;
	/*values are few, ok and much*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|Basic")
		TArray<EBehaviour> enemiesBehaviour;
	/*enter values starting left*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|Basic")
		TArray<float> valuesFlee;
	/*enter values starting left*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|Basic")
		TArray<float> valuesIgnore;
	/*enter values starting left*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|Basic")
		TArray<float> valuesAttack;

	/*set border value for behaviour intervall - noNeed*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|AmmoNeed")
		float ammoNeedBehaviourValues;
	/*values are few, ok and much*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|AmmoNeed")
		TArray<EANeedBehaviour> ammoANBehaviour;
	/*values are few, ok and much*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|AmmoNeed")
		TArray<EANeedBehaviour> healthANBehaviour;
	/*values are few, ok and much*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|AmmoNeed")
		TArray<EANeedBehaviour> enemiesANBehaviour;
	/*enter values starting left*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|AmmoNeed")
		TArray<float> valuesNoNeed;
	/*enter values starting left*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|defaultValues|Behaviour|AmmoNeed")
		TArray<float> valuesCollect;

	UFUNCTION()
		void AddActor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void RemoveActor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintPure)
		float GetAmmo();
	UFUNCTION(BlueprintPure)
		float GetHealth();
	UFUNCTION(BlueprintPure)
		float GetAccuracy();
	UFUNCTION(BlueprintPure)
		float GetBravery();
	UFUNCTION(BlueprintPure)
		float GetAmmoNeed();
	UFUNCTION(BlueprintPure)
		float GetEnemies();
	UFUNCTION(BlueprintPure)
		float GetObjects();
	UFUNCTION(BlueprintCallable)
		ETeam GetTeam();
	UFUNCTION(BlueprintPure)
		bool GetIsTargetting();
	UFUNCTION(BlueprintPure)
		bool GetIsCovering();

	UFUNCTION(BlueprintCallable, Category = "AI|FuzzyRules")
		void SetFuzzyValues();
	UFUNCTION(BlueprintCallable, Category = "AI|FuzzyInference")
		void SetValuesForAverageOfMaxima();
	UFUNCTION(BlueprintCallable, Category = "AI|FuzzyInference")
		void SetCenterOfGravity();
	UFUNCTION(BlueprintCallable, Category = "AI|Defuzzyfication")
		void SelectReaction();

	void SetAmmo(float newAmmo);
	void SetHealth(float newHealth);
	void SetTargetLocation(FVector goal);
	void SetIsIdling(bool newIdlingState);
	void SetIsCovering(bool newIsCovering);
	void GetDamage(float damage);
	UPROPERTY(EditDefaultsOnly)
		UClass* bullet;
};