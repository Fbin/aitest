// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

DECLARE_LOG_CATEGORY_EXTERN(LogAITest, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogAITestBehaviour, Log, All);

#define COLLISION_ENEMY ECC_GameTraceChannel1
#define COLLISION_PLAYER ECC_GameTraceChannel2
#define DETECTION_OBJECT ECC_GameTraceChannel3