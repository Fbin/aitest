// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BasePickup.generated.h"

UENUM()
enum class EPickup : uint8
{
	Ammo,
	Heal,
};

UCLASS()
class AITEST_API ABasePickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasePickup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	/* Returns MeshComponent subobject */
	FORCEINLINE class UStaticMeshComponent* GetMeshComponent() { return MeshComponent; }
	/* Returns MeshComponent subobject */
	FORCEINLINE class UBoxComponent* GetBoxComponent() { return BoxComponent; }

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* MeshComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* BoxComponent;
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
		float restorAmount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "defaultValues")
		EPickup pickupType;

	UFUNCTION()
		void Pickup(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
