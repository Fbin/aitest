// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "AITestGameMode.h"
#include "AITestPlayerController.h"
#include "AITestCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAITestGameMode::AAITestGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AAITestPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}