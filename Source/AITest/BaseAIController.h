// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BaseAIController.generated.h"

/**
 * 
 */
UCLASS()
class AITEST_API ABaseAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);
};
