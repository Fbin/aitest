// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

UENUM()
enum class EBehaviour : uint8
{
	Flee,
	Ignore,
	Attack,
};

UENUM()
enum class EANeedBehaviour : uint8
{
	NoNeed,
	Collect,
};

class AITEST_API Utility
{
public:
	Utility();
	~Utility();

	float LinearEquation(float x1, float x2, float y1, float y2, float input);

	float GetHighestValue(TArray<float> values);

	float GraphFunction(float minX, float peakMinX, float peakMaxX, float maxX, float maxVal, float input);

	/*position on graph 0==lowest, 1==highest*/
	float GetXValue(float minX, float peakMinX, float peakMaxX, float maxX, float maxVal, float yVal, float position);

	AActor* GetClosestTarget(FVector currentLocation, TArray<AActor*> targets);

	TArray<float> GetFuzzyValue(TArray<float> fewValues, TArray<float> okValues, TArray<float> muchValues, float maxValue, float input);

	void AssignFuzzyValues(TArray<EBehaviour> behaviours, TArray<float> &fleeValues, TArray<float> &ignoreValues, TArray<float> &attackValues, TArray<float> fuzzyValues);

	void AssignAmmoNeedFuzzyValues(TArray<EANeedBehaviour> behaviours, TArray<float> &noNeedValues, TArray<float> &collectValues, TArray<float> fuzzyValues);

	void UpdateEnemiesInView(UWorld* world, AActor* character, TArray<AActor*> &enemies, TArray<AActor*> &enemiesInView, TArray<AActor*> &enemiesLeftSight, TArray<float> &timeSinceLeft);

	void UpdateEnemiesInMind(UWorld* world, AActor* character, TArray<AActor*> &enemies, TArray<AActor*> &enemiesInView, TArray<AActor*> &enemiesLeftSight, TArray<float> &timeSinceLeft, float DeltaSeconds, float sightDistance);

	void UpdateObjectsInView(UWorld* world, AActor* character, TArray<AActor*> &objects, TArray<AActor*> &objectsInView);
};
