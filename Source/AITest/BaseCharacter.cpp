// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseCharacter.h"
#include "AITest.h"
#include "AITestPlayerController.h"
#include "BaseAIController.h"
#include "BasePickup.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Bullet.h"

// Sets default values
ABaseCharacter::ABaseCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create spherecomponent for enemy detection
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereComponent->SetupAttachment(RootComponent);

	fleeInfluenceValue = 1.0f;
	ignoreInfluenceValue = 1.0f;
	attackInfluenceValue = 1.0f;
	noNeedInfluenceValue = 1.0f;
	collectInfluenceValue = 1.0f;
	WeaponAttachPoint = TEXT("WeaponSocket");
	MuzzleAttachPoint = TEXT("MuzzleFlashSocket");
	baseShotFrequency = 0.5f;

	weaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Weapon"));
	weaponMesh->SetupAttachment(GetMesh(), WeaponAttachPoint);
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	//dont know why they had to be moved from constructor to here, but else overlap wont work with children classes
	weaponMesh->SetSkeletalMesh(gun);
	SphereComponent->SetCollisionObjectType(COLLISION_ENEMY);
	SphereComponent->SetCollisionResponseToChannel(COLLISION_PLAYER, ECR_Overlap);
	SphereComponent->SetCollisionResponseToChannel(DETECTION_OBJECT, ECR_Overlap);
	SphereComponent->SetCollisionResponseToChannel(COLLISION_ENEMY, ECR_Ignore);
	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ABaseCharacter::AddActor);
	SphereComponent->OnComponentEndOverlap.AddDynamic(this, &ABaseCharacter::RemoveActor);

	health = baseHealth;
	ammo = baseAmmo;
	accuracy = baseAccuracy;
	meleeRange = baseMeleeRange;
	shotFrequency = baseShotFrequency;
	SphereComponent->SetSphereRadius(detectionRadius);
	isTargetting = false;
	isCovering = false;
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Utility instance;
	instance.UpdateEnemiesInMind(GetWorld(), this, enemies, enemiesInView, enemiesLeftSight, timeSinceLeft, DeltaTime, detectionRadius);
	instance.UpdateEnemiesInView(GetWorld(), this, enemies, enemiesInView, enemiesLeftSight, timeSinceLeft);
	instance.UpdateObjectsInView(GetWorld(), this, objects, objectsInView);

	isTargetting = (enemiesInView.Num() + enemiesLeftSight.Num() > 0) ? true : false;

	SelectReaction();
	if (health <= 0.0f)
	{
		Destroy();
	}
}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABaseCharacter::AddActor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		if (Cast<ABaseCharacter>(OtherActor))
		{
			if (Cast<ABaseCharacter>(OtherActor)->GetTeam() != team)
			{
				enemies.AddUnique(OtherActor);
			}
			if (Cast<ABaseCharacter>(OtherActor)->GetTeam() == team)
			{
				allies.AddUnique(OtherActor);
			}
		}
		if (Cast<ABasePickup>(OtherActor))
		{
			objects.AddUnique(OtherActor);
		}
	}
}

void ABaseCharacter::RemoveActor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		if (enemies.Find(OtherActor) != INDEX_NONE)
		{
			enemies.Remove(OtherActor);
			if (enemiesInView.Find(OtherActor) != INDEX_NONE)
			{
				enemiesInView.Remove(OtherActor);
				enemiesLeftSight.AddUnique(OtherActor);
				timeSinceLeft.Add(5.0f);
			}
		}
		if (allies.Find(OtherActor) != INDEX_NONE)
		{
			allies.Remove(OtherActor);
		}
		if (objects.Find(OtherActor) != INDEX_NONE)
		{
			objects.Remove(OtherActor);
		}
	}
}

float ABaseCharacter::GetAmmo()
{
	return ammo;
}

float ABaseCharacter::GetHealth()
{
	return health;
}

float ABaseCharacter::GetAccuracy()
{
	return accuracy;
}

float ABaseCharacter::GetBravery()
{
	return bravery;
}

float ABaseCharacter::GetAmmoNeed()
{
	return ammoNeed;
}

float ABaseCharacter::GetEnemies()
{
	return enemiesInView.Num() + enemiesLeftSight.Num();
}

float ABaseCharacter::GetObjects()
{
	return objectsInView.Num();
}

ETeam ABaseCharacter::GetTeam()
{
	return team;
}

bool ABaseCharacter::GetIsTargetting()
{
	return isTargetting;
}

bool ABaseCharacter::GetIsCovering()
{
	return isCovering;
}

void ABaseCharacter::SetFuzzyValues()
{
	Utility instance;

	TArray<float> fleeValues;
	TArray<float> ignoreValues;
	TArray<float> attackValues;
	TArray<float> noNeedValues;
	TArray<float> collectValues;

	instance.AssignFuzzyValues(ammoBehaviour, fleeValues, ignoreValues, attackValues, GetFuzzyAmmo());
	instance.AssignFuzzyValues(healthBehaviour, fleeValues, ignoreValues, attackValues, GetFuzzyHealth());
	instance.AssignFuzzyValues(enemiesBehaviour, fleeValues, ignoreValues, attackValues, GetFuzzyEnemies());
	instance.AssignAmmoNeedFuzzyValues(ammoANBehaviour, noNeedValues, collectValues, GetFuzzyAmmo());
	instance.AssignAmmoNeedFuzzyValues(healthANBehaviour, noNeedValues, collectValues, GetFuzzyHealth());
	instance.AssignAmmoNeedFuzzyValues(enemiesANBehaviour, noNeedValues, collectValues, GetFuzzyEnemies());

	fleeValue = instance.GetHighestValue(fleeValues);
	ignoreValue = instance.GetHighestValue(ignoreValues);
	attackValue = instance.GetHighestValue(attackValues);
	noNeedValue = instance.GetHighestValue(noNeedValues);
	collectValue = instance.GetHighestValue(collectValues);
}

void ABaseCharacter::SetValuesForAverageOfMaxima()
{
	Utility instance;

	fleeMinX = instance.GetXValue(valuesFlee[0], valuesFlee[1], valuesFlee[2], valuesFlee[3], maxGraphValue, fleeInfluenceValue * fleeValue, 0);
	fleeMaxX = instance.GetXValue(valuesFlee[0], valuesFlee[1], valuesFlee[2], valuesFlee[3], maxGraphValue, fleeInfluenceValue * fleeValue, 1);
	ignoreMinX = instance.GetXValue(valuesIgnore[0], valuesIgnore[1], valuesIgnore[2], valuesIgnore[3], maxGraphValue, ignoreInfluenceValue * ignoreValue, 0);
	ignoreMaxX = instance.GetXValue(valuesIgnore[0], valuesIgnore[1], valuesIgnore[2], valuesIgnore[3], maxGraphValue, ignoreInfluenceValue * ignoreValue, 1);
	attackMinX = instance.GetXValue(valuesAttack[0], valuesAttack[1], valuesAttack[2], valuesAttack[3], maxGraphValue, attackInfluenceValue * attackValue, 0);
	attackMaxX = instance.GetXValue(valuesAttack[0], valuesAttack[1], valuesAttack[2], valuesAttack[3], maxGraphValue, attackInfluenceValue * attackValue, 1);
	noNeedMinX = instance.GetXValue(valuesNoNeed[0], valuesNoNeed[1], valuesNoNeed[2], valuesNoNeed[3], maxGraphValue, noNeedInfluenceValue * noNeedValue, 0);
	noNeedMaxX = instance.GetXValue(valuesNoNeed[0], valuesNoNeed[1], valuesNoNeed[2], valuesNoNeed[3], maxGraphValue, noNeedInfluenceValue * noNeedValue, 1);
	collectMinX = instance.GetXValue(valuesCollect[0], valuesCollect[1], valuesCollect[2], valuesCollect[3], maxGraphValue, collectInfluenceValue * collectValue, 0);
	collectMaxX = instance.GetXValue(valuesCollect[0], valuesCollect[1], valuesCollect[2], valuesCollect[3], maxGraphValue, collectInfluenceValue * collectValue, 1);
}

void ABaseCharacter::SetCenterOfGravity()
{
	SetFuzzyValues();
	SetValuesForAverageOfMaxima();
	bravery = (fleeValue*((fleeMinX + fleeMaxX) / 2) + ignoreValue*((ignoreMinX + ignoreMaxX) / 2) + attackValue*((attackMinX + attackMaxX) / 2)) / (fleeValue + ignoreValue + attackValue);
	ammoNeed = (noNeedValue*((noNeedMinX + noNeedMaxX) / 2) + collectValue*((collectMinX + collectMaxX) / 2)) / (noNeedValue + collectValue);
}

void ABaseCharacter::SelectReaction()
{
	SetCenterOfGravity();
	if (bravery <= behaviourValues[1])
	{
		if (bravery <= behaviourValues[0])
		{
			Flee();
			//UE_LOG(LogAITestBehaviour, Warning, TEXT("Flee, value: %f"), v);
		}
		else
		{
			consideredTargets.Empty();
			for (int i = 0; i < enemiesInView.Num(); i++)
			{
				if (FVector::DotProduct(GetActorForwardVector(), enemiesInView[i]->GetActorForwardVector()) < 0.0f)
				{
					consideredTargets.Add(enemiesInView[i]);
				}
			}
			if (consideredTargets.Num() > 0)
			{
				executeAttack(consideredTargets);
			}
			Idling(50.0f, 300.0f, 2.0f, 5.0f);
			//UE_LOG(LogAITestBehaviour, Warning, TEXT("Idling, value: %f"), v);
		}
	}
	else
	{
		Attack();
		//UE_LOG(LogAITestBehaviour, Warning, TEXT("Attack, value: %f"), v);
	}
}

void ABaseCharacter::Attack()
{
	if (enemiesInView.Num() > 0)
	{
		executeAttack(enemiesInView);
	}
	else
	{
		Idling(200.0f, 600.0f, 1.0f, 2.0f);
	}
}

void ABaseCharacter::executeAttack(TArray<AActor*> targets)
{
	Utility instance;
	AActor* target = instance.GetClosestTarget(GetActorLocation(), targets);

	if (ammo > 0.0f)
	{
		if (!hasShot)
		{
			GetWorld()->GetTimerManager().SetTimer(ShotTimerHandle, this, &ABaseCharacter::EndShot, shotFrequency, false);
			hasShot = true;
			Shoot(target);
			//UE_LOG(LogAITestBehaviour, Warning, TEXT("AITestCharacter, Shothit"));
		}
	}
	else
	{
		if ((target->GetActorLocation() - GetActorLocation()).Size() <= meleeRange)
		{
			DealDamage(target);
			//UE_LOG(LogAITestBehaviour, Warning, TEXT("AITestCharacter, Meleehit"));
		}
		else
		{
			targetLocation = target->GetActorLocation();
			if (Cast<AAITestPlayerController>(GetController()))
			{
				Cast<AAITestPlayerController>(GetController())->SetNewMoveDestination(targetLocation);
			}
			else
			{
				Cast<ABaseAIController>(GetController())->SetNewMoveDestination(targetLocation);
				//UE_LOG(LogAITestBehaviour, Warning, TEXT("BaseCharacter.cpp: AI, Attack"));
			}
		}
	}
}

void ABaseCharacter::Idling(float minRange, float maxRange, float minWaitTime, float maxWaitTime)
{
	if (!isIdling)
	{
		if (ammoNeed >= ammoNeedBehaviourValues && objectsInView.Num() > 0)
		{
			Utility instance;
			AActor* target = instance.GetClosestTarget(GetActorLocation(), objectsInView);
			targetLocation = target->GetActorLocation();
			//UE_LOG(LogAITestBehaviour, Warning, TEXT("BaseCharacter.cpp: Pickup Searching"));
		}
		else
		{
			GetWorld()->GetTimerManager().SetTimer(IdleTimerHandle, this, &ABaseCharacter::EndIdleWait, FMath::FRandRange(minWaitTime, maxWaitTime), false);
			isIdling = true;
			float moveDist = FMath::FRandRange(minRange, maxRange);
			FRotator angle = FRotator(0.0f, FMath::FloorToInt(FMath::FRandRange(0.0f, 360.0f)), 0.0f);
			targetLocation = angle.RotateVector(GetActorForwardVector())*moveDist + GetActorLocation();
		}
	}

	if (Cast<AAITestPlayerController>(GetController()))
	{
		Cast<AAITestPlayerController>(GetController())->SetNewMoveDestination(targetLocation);
	}
	else
	{
		Cast<ABaseAIController>(GetController())->SetNewMoveDestination(targetLocation);
		//UE_LOG(LogAITestBehaviour, Warning, TEXT("BaseCharacter.cpp: AI, Idle"));
	}
}

void ABaseCharacter::Flee()
{
	FVector targetLoc;

	if (enemies.Num() >= 1)
	{
		targetLoc = enemies[0]->GetActorLocation();
		for (int i = 1; i < enemies.Num(); i++)
		{
			FVector old = targetLoc - GetActorLocation();
			FVector next = enemies[i]->GetActorLocation() - GetActorLocation();
			if (old.Size() < next.Size())
			{
				targetLoc = enemies[i]->GetActorLocation();
			}
		}
		targetLocation = GetActorLocation() + FMath::FRandRange(0.1f, 1.0f) * GetActorLocation() - targetLoc;

		if (Cast<AAITestPlayerController>(GetController()))
		{
			Cast<AAITestPlayerController>(GetController())->SetNewMoveDestination(targetLocation);
		}
		else
		{
			Cast<ABaseAIController>(GetController())->SetNewMoveDestination(targetLocation);
			//UE_LOG(LogAITestBehaviour, Warning, TEXT("BaseCharacter.cpp: AI, Flee"));
		}
	}
	else
	{
		Idling(50.0f, 150.0f, 3.0f, 7.0f);
	}
}

void ABaseCharacter::SetTargetLocation(FVector goal)
{
	targetLocation = goal;
}

void ABaseCharacter::EndIdleWait()
{
	isIdling = false;
}

void ABaseCharacter::EndShot()
{
	hasShot = false;
}

void ABaseCharacter::EndCover()
{
	isCovering = false;
}

void ABaseCharacter::DealDamage(AActor* target)
{
	float h = Cast<ABaseCharacter>(target)->GetHealth();
	float damage = FMath::FRandRange(1.0f, 10.0f);
	if (Cast<ABaseCharacter>(target)->GetIsCovering())
	{
		damage *= 0.8f;
	}
	Cast<ABaseCharacter>(target)->GetDamage(damage);
	Cast<ABaseCharacter>(target)->SetIsCovering(true);
	if (h <= 0.0f)
	{
		enemies.Remove(target);
		enemiesInView.Remove(target);
	}

	UGameplayStatics::SpawnEmitterAttached(Cast<ABaseCharacter>(target)->hitParticles, Cast<ABaseCharacter>(target)->GetMesh(), NAME_None, target->GetActorLocation(), UKismetMathLibrary::MakeRotationFromAxes(target->GetActorLocation()-GetActorLocation(), GetActorRightVector(), GetActorUpVector()), EAttachLocation::KeepWorldPosition);
}

void ABaseCharacter::GetDamage(float damage)
{
	SetHealth(-damage);
	GetWorld()->GetTimerManager().SetTimer(CoverTimerHandle, this, &ABaseCharacter::EndCover, 5.0f, false);
}

void ABaseCharacter::Shoot(AActor* target)
{
	ammo--;
	FHitResult HitResult;
	FCollisionQueryParams Params(TEXT("HitTrace"), true, this);
	FVector StartLocation = weaponMesh->GetSocketLocation(MuzzleAttachPoint);
	FVector targetLocation;
	UGameplayStatics::SpawnEmitterAttached(muzzleFlashParticles, weaponMesh, MuzzleAttachPoint);
	FVector forward = (target->GetActorLocation() - StartLocation)*FVector(1.0f, 1.0f, 0.0f);
	FVector up = GetActorUpVector();
	FVector right = FVector::CrossProduct(up, forward);
	SetActorRotation(UKismetMathLibrary::MakeRotationFromAxes(forward, right, up));
	FRotator rot = GetActorRotation();
	AActor* newActor = GetWorld()->SpawnActor(bullet, &StartLocation, &rot);
	if (FMath::FRand() <= accuracy)
	{
		DealDamage(target);
		targetLocation = target->GetActorLocation();
		//DrawDebugLine(GetWorld(), StartLocation, targetLocation, FColor::Green, false, 0.5f);
	}
	else
	{
		targetLocation = target->GetActorLocation() + FVector(FMath::FRandRange(50.0f, 100.0f), FMath::FRandRange(50.0f, 100.0f), 0.0f);
		//DrawDebugLine(GetWorld(), StartLocation, targetLocation, FColor::Red, false, 0.5f);
	}
	Cast<ABullet>(newActor)->SetForwardVector(forward/forward.Size());
}

TArray<float> ABaseCharacter::GetFuzzyAmmo()
{
	Utility instance;
	return instance.GetFuzzyValue(valuesAFew, valuesAOk, valuesAMuch, maxGraphValue, ammo);
}

TArray<float> ABaseCharacter::GetFuzzyHealth()
{
	Utility instance;
	return instance.GetFuzzyValue(valuesHFew, valuesHOk, valuesHMuch, maxGraphValue, health);
}

TArray<float> ABaseCharacter::GetFuzzyEnemies()
{
	Utility instance;
	return instance.GetFuzzyValue(valuesEFew, valuesEOk, valuesEMuch, maxGraphValue, (enemiesInView.Num() + enemiesLeftSight.Num()) / (allies.Num() + 1));
}

void ABaseCharacter::SetAmmo(float newAmmo)
{
	ammo = FMath::Clamp(ammo + newAmmo, 0.0f, 100.0f);
}

void ABaseCharacter::SetHealth(float newHealth)
{
	health = FMath::Clamp(health + newHealth, 0.0f, 100.0f);
}

void ABaseCharacter::SetIsIdling(bool newIdlingState)
{
	isIdling = newIdlingState;
}

void ABaseCharacter::SetIsCovering(bool newIsCovering)
{
	isCovering = newIsCovering;
}