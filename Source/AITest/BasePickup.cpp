// Fill out your copyright notice in the Description page of Project Settings.

#include "BasePickup.h"
#include "Components/StaticMeshComponent.h"
#include "BaseCharacter.h"
#include "BaseAIController.h"
#include "AITestPlayerController.h"
#include "AITestCharacter.h"
#include "AICharacter.h"
#include "AITest.h"


// Sets default values
ABasePickup::ABasePickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;
	MeshComponent->SetCollisionObjectType(COLLISION_PLAYER);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshComponent->SetCollisionResponseToChannel(COLLISION_PLAYER, ECR_Block);
	MeshComponent->SetCollisionResponseToChannel(COLLISION_ENEMY, ECR_Ignore);
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABasePickup::Pickup);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Collisionbox"));
	BoxComponent->SetupAttachment(RootComponent);
	BoxComponent->SetCollisionObjectType(DETECTION_OBJECT);
	BoxComponent->SetCollisionResponseToChannel(COLLISION_ENEMY, ECR_Overlap);
	BoxComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECR_Block);

	/*false so navmesh is not influenced and characters can move into it*/
	MeshComponent->SetCanEverAffectNavigation(false);
}

// Called when the game starts or when spawned
void ABasePickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasePickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABasePickup::Pickup(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	switch (pickupType)
	{
	case EPickup::Ammo:
		Cast<ABaseCharacter>(OtherActor)->SetAmmo(restorAmount);
		break;
	case EPickup::Heal:
		Cast<ABaseCharacter>(OtherActor)->SetHealth(restorAmount);
		break;
	}

	if (Cast<AAITestPlayerController>(Cast<ABaseCharacter>(OtherActor)->GetController()))
	{
		Cast<AAITestPlayerController>(Cast<ABaseCharacter>(OtherActor)->GetController())->SetNewMoveDestination(FVector(FMath::FRandRange(150.0f, 250.0f), FMath::FRandRange(150.0f, 250.0f), 0.0f) + OtherActor->GetActorLocation());
	}
	if (Cast<ABaseAIController>(Cast<ABaseCharacter>(OtherActor)->GetController()))
	{
		Cast<ABaseAIController>(Cast<ABaseCharacter>(OtherActor)->GetController())->SetNewMoveDestination(FVector(FMath::FRandRange(150.0f, 250.0f), FMath::FRandRange(150.0f, 250.0f), 0.0f) + OtherActor->GetActorLocation());
	}
}