// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "AITest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, AITest, "AITest" );

DEFINE_LOG_CATEGORY(LogAITest)
DEFINE_LOG_CATEGORY(LogAITestBehaviour)